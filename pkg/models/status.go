/*
2019 © Postgres.ai
*/

package models

type Status struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}
